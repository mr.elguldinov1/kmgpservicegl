﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KMGService.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;

namespace KMGService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NoteController : ControllerBase
    {
        private readonly MainContext _db;

        public NoteController(MainContext db)
        {
            _db = db;
        }

        // GET: api/Note
        [HttpGet("List")]
        public async Task<IEnumerable<Note>> Get()
        {
            return await _db.Notes.ToListAsync();
        }

        // GET: api/Note/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<Note> Get(int? id)
        {
            if (id != null)
            {
                return await _db.Notes.FindAsync(id);
            }
            return null;
        }

        // POST: api/Note
        [HttpPost]
        public async Task<IActionResult> Post(Note value)
        {
            if (value != null)
            {
                _db.Entry(value).State = EntityState.Added;
                await _db.SaveChangesAsync();
                return Ok();
            }
            return BadRequest();
        }

        // PUT: api/Note/5
        [HttpPut]
        public async Task Put(Note value)
        {
            if (value != null)
            {
                _db.Entry(value).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task Delete(int? id)
        {
            if (id != null)
            {
                var note = await _db.Notes.FindAsync(id);
                _db.Entry(note).State = EntityState.Deleted;
                await _db.SaveChangesAsync();
            }
        }
    }
}
