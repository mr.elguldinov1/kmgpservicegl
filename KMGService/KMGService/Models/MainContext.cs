﻿using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KMGService.Models
{
    public class MainContext: DbContext
    {
        public MainContext(DbContextOptions<MainContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=KMGP.db");
        }

        public DbSet<Note> Notes { get; set; }
    }
}
