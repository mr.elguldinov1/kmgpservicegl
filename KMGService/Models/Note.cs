﻿using System;

namespace Models
{
    public class Note
    {
        public Note()
        {
        }

        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Context { get; set; }
    }
}
